package scripts
/**
 * Created by raitis on 31/07/15.
 */
object RunGen extends App {
  val slickDriver = "slick.driver.MySQLDriver"
  val jdbcDriver = "com.mysql.jdbc.Driver"
  val url = "jdbc:mysql://localhost/slick_intro"
  val outputFolder ="app"
  val pkg = "models.auto_generated"
  val user = "root"
  val password = ""


  slick.codegen.SourceCodeGenerator.main(
    Array(slickDriver, jdbcDriver, url, outputFolder, pkg, user, password)
  )
}

