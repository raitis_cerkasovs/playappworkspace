package models.auto_generated
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.driver.MySQLDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.driver.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema = Authorities.schema ++ Users.schema
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table Authorities
   *  @param id Database column ID SqlType(INT), PrimaryKey
   *  @param idUser Database column ID_USER SqlType(INT)
   *  @param authority Database column AUTHORITY SqlType(VARCHAR), Length(254,true) */
  case class AuthoritiesRow(id: Int, idUser: Int, authority: String)
  /** GetResult implicit for fetching AuthoritiesRow objects using plain SQL queries */
  implicit def GetResultAuthoritiesRow(implicit e0: GR[Int], e1: GR[String]): GR[AuthoritiesRow] = GR{
    prs => import prs._
    AuthoritiesRow.tupled((<<[Int], <<[Int], <<[String]))
  }
  /** Table description of table AUTHORITIES. Objects of this class serve as prototypes for rows in queries. */
  class Authorities(_tableTag: Tag) extends Table[AuthoritiesRow](_tableTag, "AUTHORITIES") {
    def * = (id, idUser, authority) <> (AuthoritiesRow.tupled, AuthoritiesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(idUser), Rep.Some(authority)).shaped.<>({r=>import r._; _1.map(_=> AuthoritiesRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column ID SqlType(INT), PrimaryKey */
    val id: Rep[Int] = column[Int]("ID", O.PrimaryKey)
    /** Database column ID_USER SqlType(INT) */
    val idUser: Rep[Int] = column[Int]("ID_USER")
    /** Database column AUTHORITY SqlType(VARCHAR), Length(254,true) */
    val authority: Rep[String] = column[String]("AUTHORITY", O.Length(254,varying=true))

    /** Foreign key referencing Users (database name AUTH_FK) */
    lazy val usersFk = foreignKey("AUTH_FK", idUser, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Authorities */
  lazy val Authorities = new TableQuery(tag => new Authorities(tag))

  /** Entity class storing rows of table Users
   *  @param id Database column ID SqlType(INT), PrimaryKey
   *  @param name Database column NAME SqlType(VARCHAR), Length(254,true)
   *  @param password Database column PASSWORD SqlType(VARCHAR), Length(254,true) */
  case class UsersRow(id: Int, name: String, password: String)
  /** GetResult implicit for fetching UsersRow objects using plain SQL queries */
  implicit def GetResultUsersRow(implicit e0: GR[Int], e1: GR[String]): GR[UsersRow] = GR{
    prs => import prs._
    UsersRow.tupled((<<[Int], <<[String], <<[String]))
  }
  /** Table description of table USERS. Objects of this class serve as prototypes for rows in queries. */
  class Users(_tableTag: Tag) extends Table[UsersRow](_tableTag, "USERS") {
    def * = (id, name, password) <> (UsersRow.tupled, UsersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(name), Rep.Some(password)).shaped.<>({r=>import r._; _1.map(_=> UsersRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column ID SqlType(INT), PrimaryKey */
    val id: Rep[Int] = column[Int]("ID", O.PrimaryKey)
    /** Database column NAME SqlType(VARCHAR), Length(254,true) */
    val name: Rep[String] = column[String]("NAME", O.Length(254,varying=true))
    /** Database column PASSWORD SqlType(VARCHAR), Length(254,true) */
    val password: Rep[String] = column[String]("PASSWORD", O.Length(254,varying=true))
  }
  /** Collection-like TableQuery object for table Users */
  lazy val Users = new TableQuery(tag => new Users(tag))
}
