name := """App-Slick-3.0"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  specs2 % Test
)

libraryDependencies += "com.typesafe.slick" %% "slick" % "3.0.0"
libraryDependencies += "com.typesafe.slick" %% "slick-codegen" % "3.0.0"
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.12"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

