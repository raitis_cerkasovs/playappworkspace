package model

import play.api.libs.json.Json

/**
 * Created by raitis on 16/07/15.
 */
case class Person (name: String)

object Person {

  implicit val personFormat = Json.format[Person]

}
