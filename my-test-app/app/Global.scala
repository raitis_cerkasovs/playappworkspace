import play.api.{Application, GlobalSettings}



/**
 * Created by raitis on 03/08/15.
 */
object Global extends GlobalSettings  {

  override def onStart(app: Application): Unit =  {
    InitialData.start()
  }
}


object InitialData {

  def start(): Unit = {
    println("Hi Ray, App Started!")
  }

}