package models.auto_generated
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.driver.MySQLDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.driver.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema = Array(Authorities.schema, Clients.schema, Couriers.schema, Feedbacks.schema, Jobs.schema, Messages.schema, Sendfrom.schema, Sendto.schema, Users.schema).reduceLeft(_ ++ _)
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table Authorities
   *  @param authority Database column authority SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param text Database column text SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param username Database column username SqlType(VARCHAR), PrimaryKey, Length(254,true) */
  case class AuthoritiesRow(authority: Option[String] = None, text: Option[String] = None, username: String)
  /** GetResult implicit for fetching AuthoritiesRow objects using plain SQL queries */
  implicit def GetResultAuthoritiesRow(implicit e0: GR[Option[String]], e1: GR[String]): GR[AuthoritiesRow] = GR{
    prs => import prs._
    AuthoritiesRow.tupled((<<?[String], <<?[String], <<[String]))
  }
  /** Table description of table authorities. Objects of this class serve as prototypes for rows in queries. */
  class Authorities(_tableTag: Tag) extends Table[AuthoritiesRow](_tableTag, "authorities") {
    def * = (authority, text, username) <> (AuthoritiesRow.tupled, AuthoritiesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (authority, text, Rep.Some(username)).shaped.<>({r=>import r._; _3.map(_=> AuthoritiesRow.tupled((_1, _2, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column authority SqlType(VARCHAR), Length(45,true), Default(None) */
    val authority: Rep[Option[String]] = column[Option[String]]("authority", O.Length(45,varying=true), O.Default(None))
    /** Database column text SqlType(VARCHAR), Length(45,true), Default(None) */
    val text: Rep[Option[String]] = column[Option[String]]("text", O.Length(45,varying=true), O.Default(None))
    /** Database column username SqlType(VARCHAR), PrimaryKey, Length(254,true) */
    val username: Rep[String] = column[String]("username", O.PrimaryKey, O.Length(254,varying=true))

    /** Foreign key referencing Users (database name fk_authorities_users1) */
    lazy val usersFk = foreignKey("fk_authorities_users1", username, Users)(r => r.username, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Authorities */
  lazy val Authorities = new TableQuery(tag => new Authorities(tag))

  /** Entity class storing rows of table Clients
   *  @param bankid Database column bankid SqlType(INT), Default(None)
   *  @param text1 Database column text1 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param text2 Database column text2 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param username Database column username SqlType(VARCHAR), PrimaryKey, Length(254,true) */
  case class ClientsRow(bankid: Option[Int] = None, text1: Option[String] = None, text2: Option[String] = None, username: String)
  /** GetResult implicit for fetching ClientsRow objects using plain SQL queries */
  implicit def GetResultClientsRow(implicit e0: GR[Option[Int]], e1: GR[Option[String]], e2: GR[String]): GR[ClientsRow] = GR{
    prs => import prs._
    ClientsRow.tupled((<<?[Int], <<?[String], <<?[String], <<[String]))
  }
  /** Table description of table clients. Objects of this class serve as prototypes for rows in queries. */
  class Clients(_tableTag: Tag) extends Table[ClientsRow](_tableTag, "clients") {
    def * = (bankid, text1, text2, username) <> (ClientsRow.tupled, ClientsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (bankid, text1, text2, Rep.Some(username)).shaped.<>({r=>import r._; _4.map(_=> ClientsRow.tupled((_1, _2, _3, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column bankid SqlType(INT), Default(None) */
    val bankid: Rep[Option[Int]] = column[Option[Int]]("bankid", O.Default(None))
    /** Database column text1 SqlType(VARCHAR), Length(254,true), Default(None) */
    val text1: Rep[Option[String]] = column[Option[String]]("text1", O.Length(254,varying=true), O.Default(None))
    /** Database column text2 SqlType(VARCHAR), Length(254,true), Default(None) */
    val text2: Rep[Option[String]] = column[Option[String]]("text2", O.Length(254,varying=true), O.Default(None))
    /** Database column username SqlType(VARCHAR), PrimaryKey, Length(254,true) */
    val username: Rep[String] = column[String]("username", O.PrimaryKey, O.Length(254,varying=true))

    /** Foreign key referencing Users (database name fk_clients_users1) */
    lazy val usersFk = foreignKey("fk_clients_users1", username, Users)(r => r.username, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Clients */
  lazy val Clients = new TableQuery(tag => new Clients(tag))

  /** Entity class storing rows of table Couriers
   *  @param `type` Database column type SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param isworking Database column isworking SqlType(BIT), Default(None)
   *  @param isavailable Database column isavailable SqlType(BIT), Default(None)
   *  @param lat Database column lat SqlType(DECIMAL), Default(None)
   *  @param lon Database column lon SqlType(DECIMAL), Default(None)
   *  @param bankid Database column bankid SqlType(INT), Default(None)
   *  @param text1 Database column text1 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param text2 Database column text2 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param username Database column username SqlType(VARCHAR), PrimaryKey, Length(254,true) */
  case class CouriersRow(`type`: Option[String] = None, isworking: Option[Boolean] = None, isavailable: Option[Boolean] = None, lat: Option[scala.math.BigDecimal] = None, lon: Option[scala.math.BigDecimal] = None, bankid: Option[Int] = None, text1: Option[String] = None, text2: Option[String] = None, username: String)
  /** GetResult implicit for fetching CouriersRow objects using plain SQL queries */
  implicit def GetResultCouriersRow(implicit e0: GR[Option[String]], e1: GR[Option[Boolean]], e2: GR[Option[scala.math.BigDecimal]], e3: GR[Option[Int]], e4: GR[String]): GR[CouriersRow] = GR{
    prs => import prs._
    CouriersRow.tupled((<<?[String], <<?[Boolean], <<?[Boolean], <<?[scala.math.BigDecimal], <<?[scala.math.BigDecimal], <<?[Int], <<?[String], <<?[String], <<[String]))
  }
  /** Table description of table couriers. Objects of this class serve as prototypes for rows in queries.
   *  NOTE: The following names collided with Scala keywords and were escaped: type */
  class Couriers(_tableTag: Tag) extends Table[CouriersRow](_tableTag, "couriers") {
    def * = (`type`, isworking, isavailable, lat, lon, bankid, text1, text2, username) <> (CouriersRow.tupled, CouriersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (`type`, isworking, isavailable, lat, lon, bankid, text1, text2, Rep.Some(username)).shaped.<>({r=>import r._; _9.map(_=> CouriersRow.tupled((_1, _2, _3, _4, _5, _6, _7, _8, _9.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column type SqlType(VARCHAR), Length(45,true), Default(None)
     *  NOTE: The name was escaped because it collided with a Scala keyword. */
    val `type`: Rep[Option[String]] = column[Option[String]]("type", O.Length(45,varying=true), O.Default(None))
    /** Database column isworking SqlType(BIT), Default(None) */
    val isworking: Rep[Option[Boolean]] = column[Option[Boolean]]("isworking", O.Default(None))
    /** Database column isavailable SqlType(BIT), Default(None) */
    val isavailable: Rep[Option[Boolean]] = column[Option[Boolean]]("isavailable", O.Default(None))
    /** Database column lat SqlType(DECIMAL), Default(None) */
    val lat: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("lat", O.Default(None))
    /** Database column lon SqlType(DECIMAL), Default(None) */
    val lon: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("lon", O.Default(None))
    /** Database column bankid SqlType(INT), Default(None) */
    val bankid: Rep[Option[Int]] = column[Option[Int]]("bankid", O.Default(None))
    /** Database column text1 SqlType(VARCHAR), Length(254,true), Default(None) */
    val text1: Rep[Option[String]] = column[Option[String]]("text1", O.Length(254,varying=true), O.Default(None))
    /** Database column text2 SqlType(VARCHAR), Length(254,true), Default(None) */
    val text2: Rep[Option[String]] = column[Option[String]]("text2", O.Length(254,varying=true), O.Default(None))
    /** Database column username SqlType(VARCHAR), PrimaryKey, Length(254,true) */
    val username: Rep[String] = column[String]("username", O.PrimaryKey, O.Length(254,varying=true))

    /** Foreign key referencing Users (database name fk_couriers_users1) */
    lazy val usersFk = foreignKey("fk_couriers_users1", username, Users)(r => r.username, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Couriers */
  lazy val Couriers = new TableQuery(tag => new Couriers(tag))

  /** Entity class storing rows of table Feedbacks
   *  @param id Database column id SqlType(INT), AutoInc, PrimaryKey
   *  @param text Database column text SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param created Database column created SqlType(TIMESTAMP), Default(None)
   *  @param visible Database column visible SqlType(BIT), Default(None)
   *  @param courier Database column courier SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param client Database column client SqlType(VARCHAR), Length(254,true)
   *  @param job Database column job SqlType(INT) */
  case class FeedbacksRow(id: Int, text: Option[String] = None, created: Option[java.sql.Timestamp] = None, visible: Option[Boolean] = None, courier: Option[String] = None, client: String, job: Int)
  /** GetResult implicit for fetching FeedbacksRow objects using plain SQL queries */
  implicit def GetResultFeedbacksRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[Option[java.sql.Timestamp]], e3: GR[Option[Boolean]], e4: GR[String]): GR[FeedbacksRow] = GR{
    prs => import prs._
    FeedbacksRow.tupled((<<[Int], <<?[String], <<?[java.sql.Timestamp], <<?[Boolean], <<?[String], <<[String], <<[Int]))
  }
  /** Table description of table feedbacks. Objects of this class serve as prototypes for rows in queries. */
  class Feedbacks(_tableTag: Tag) extends Table[FeedbacksRow](_tableTag, "feedbacks") {
    def * = (id, text, created, visible, courier, client, job) <> (FeedbacksRow.tupled, FeedbacksRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), text, created, visible, courier, Rep.Some(client), Rep.Some(job)).shaped.<>({r=>import r._; _1.map(_=> FeedbacksRow.tupled((_1.get, _2, _3, _4, _5, _6.get, _7.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column text SqlType(VARCHAR), Length(254,true), Default(None) */
    val text: Rep[Option[String]] = column[Option[String]]("text", O.Length(254,varying=true), O.Default(None))
    /** Database column created SqlType(TIMESTAMP), Default(None) */
    val created: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("created", O.Default(None))
    /** Database column visible SqlType(BIT), Default(None) */
    val visible: Rep[Option[Boolean]] = column[Option[Boolean]]("visible", O.Default(None))
    /** Database column courier SqlType(VARCHAR), Length(254,true), Default(None) */
    val courier: Rep[Option[String]] = column[Option[String]]("courier", O.Length(254,varying=true), O.Default(None))
    /** Database column client SqlType(VARCHAR), Length(254,true) */
    val client: Rep[String] = column[String]("client", O.Length(254,varying=true))
    /** Database column job SqlType(INT) */
    val job: Rep[Int] = column[Int]("job")

    /** Foreign key referencing Clients (database name fk_feedbacks_clients1) */
    lazy val clientsFk = foreignKey("fk_feedbacks_clients1", client, Clients)(r => r.username, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Couriers (database name fk_feedbacks_couriers1) */
    lazy val couriersFk = foreignKey("fk_feedbacks_couriers1", courier, Couriers)(r => Rep.Some(r.username), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Jobs (database name fk_feedbacks_jobs1) */
    lazy val jobsFk = foreignKey("fk_feedbacks_jobs1", job, Jobs)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Feedbacks */
  lazy val Feedbacks = new TableQuery(tag => new Feedbacks(tag))

  /** Entity class storing rows of table Jobs
   *  @param id Database column id SqlType(INT), AutoInc, PrimaryKey
   *  @param distance Database column distance SqlType(FLOAT), Default(None)
   *  @param status Database column status SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param price Database column price SqlType(FLOAT), Default(None)
   *  @param notes Database column notes SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param sendfrom Database column sendfrom SqlType(INT)
   *  @param sendto Database column sendto SqlType(INT)
   *  @param visible Database column visible SqlType(BIT), Default(None)
   *  @param coltimefrom Database column coltimefrom SqlType(TIME), Default(None)
   *  @param coltimetill Database column coltimetill SqlType(TIME), Default(None)
   *  @param coldate Database column coldate SqlType(DATETIME), Default(None)
   *  @param deltimefrom Database column deltimefrom SqlType(TIME), Default(None)
   *  @param deltimetill Database column deltimetill SqlType(TIME), Default(None)
   *  @param deldate Database column deldate SqlType(DATETIME), Default(None)
   *  @param created Database column created SqlType(TIMESTAMP), Default(None)
   *  @param courier Database column courier SqlType(VARCHAR), Length(254,true)
   *  @param client Database column client SqlType(VARCHAR), Length(254,true) */
  case class JobsRow(id: Int, distance: Option[Float] = None, status: Option[String] = None, price: Option[Float] = None, notes: Option[String] = None, sendfrom: Int, sendto: Int, visible: Option[Boolean] = None, coltimefrom: Option[java.sql.Time] = None, coltimetill: Option[java.sql.Time] = None, coldate: Option[java.sql.Timestamp] = None, deltimefrom: Option[java.sql.Time] = None, deltimetill: Option[java.sql.Time] = None, deldate: Option[java.sql.Timestamp] = None, created: Option[java.sql.Timestamp] = None, courier: String, client: String)
  /** GetResult implicit for fetching JobsRow objects using plain SQL queries */
  implicit def GetResultJobsRow(implicit e0: GR[Int], e1: GR[Option[Float]], e2: GR[Option[String]], e3: GR[Option[Boolean]], e4: GR[Option[java.sql.Time]], e5: GR[Option[java.sql.Timestamp]], e6: GR[String]): GR[JobsRow] = GR{
    prs => import prs._
    JobsRow.tupled((<<[Int], <<?[Float], <<?[String], <<?[Float], <<?[String], <<[Int], <<[Int], <<?[Boolean], <<?[java.sql.Time], <<?[java.sql.Time], <<?[java.sql.Timestamp], <<?[java.sql.Time], <<?[java.sql.Time], <<?[java.sql.Timestamp], <<?[java.sql.Timestamp], <<[String], <<[String]))
  }
  /** Table description of table jobs. Objects of this class serve as prototypes for rows in queries. */
  class Jobs(_tableTag: Tag) extends Table[JobsRow](_tableTag, "jobs") {
    def * = (id, distance, status, price, notes, sendfrom, sendto, visible, coltimefrom, coltimetill, coldate, deltimefrom, deltimetill, deldate, created, courier, client) <> (JobsRow.tupled, JobsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), distance, status, price, notes, Rep.Some(sendfrom), Rep.Some(sendto), visible, coltimefrom, coltimetill, coldate, deltimefrom, deltimetill, deldate, created, Rep.Some(courier), Rep.Some(client)).shaped.<>({r=>import r._; _1.map(_=> JobsRow.tupled((_1.get, _2, _3, _4, _5, _6.get, _7.get, _8, _9, _10, _11, _12, _13, _14, _15, _16.get, _17.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column distance SqlType(FLOAT), Default(None) */
    val distance: Rep[Option[Float]] = column[Option[Float]]("distance", O.Default(None))
    /** Database column status SqlType(VARCHAR), Length(45,true), Default(None) */
    val status: Rep[Option[String]] = column[Option[String]]("status", O.Length(45,varying=true), O.Default(None))
    /** Database column price SqlType(FLOAT), Default(None) */
    val price: Rep[Option[Float]] = column[Option[Float]]("price", O.Default(None))
    /** Database column notes SqlType(VARCHAR), Length(254,true), Default(None) */
    val notes: Rep[Option[String]] = column[Option[String]]("notes", O.Length(254,varying=true), O.Default(None))
    /** Database column sendfrom SqlType(INT) */
    val sendfrom: Rep[Int] = column[Int]("sendfrom")
    /** Database column sendto SqlType(INT) */
    val sendto: Rep[Int] = column[Int]("sendto")
    /** Database column visible SqlType(BIT), Default(None) */
    val visible: Rep[Option[Boolean]] = column[Option[Boolean]]("visible", O.Default(None))
    /** Database column coltimefrom SqlType(TIME), Default(None) */
    val coltimefrom: Rep[Option[java.sql.Time]] = column[Option[java.sql.Time]]("coltimefrom", O.Default(None))
    /** Database column coltimetill SqlType(TIME), Default(None) */
    val coltimetill: Rep[Option[java.sql.Time]] = column[Option[java.sql.Time]]("coltimetill", O.Default(None))
    /** Database column coldate SqlType(DATETIME), Default(None) */
    val coldate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("coldate", O.Default(None))
    /** Database column deltimefrom SqlType(TIME), Default(None) */
    val deltimefrom: Rep[Option[java.sql.Time]] = column[Option[java.sql.Time]]("deltimefrom", O.Default(None))
    /** Database column deltimetill SqlType(TIME), Default(None) */
    val deltimetill: Rep[Option[java.sql.Time]] = column[Option[java.sql.Time]]("deltimetill", O.Default(None))
    /** Database column deldate SqlType(DATETIME), Default(None) */
    val deldate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("deldate", O.Default(None))
    /** Database column created SqlType(TIMESTAMP), Default(None) */
    val created: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("created", O.Default(None))
    /** Database column courier SqlType(VARCHAR), Length(254,true) */
    val courier: Rep[String] = column[String]("courier", O.Length(254,varying=true))
    /** Database column client SqlType(VARCHAR), Length(254,true) */
    val client: Rep[String] = column[String]("client", O.Length(254,varying=true))

    /** Foreign key referencing Clients (database name fk_jobs_clients1) */
    lazy val clientsFk = foreignKey("fk_jobs_clients1", client, Clients)(r => r.username, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Couriers (database name fk_jobs_couriers1) */
    lazy val couriersFk = foreignKey("fk_jobs_couriers1", courier, Couriers)(r => r.username, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Sendfrom (database name fk_jobs_sendfrom1) */
    lazy val sendfromFk = foreignKey("fk_jobs_sendfrom1", sendfrom, Sendfrom)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Sendto (database name fk_jobs_sendto1) */
    lazy val sendtoFk = foreignKey("fk_jobs_sendto1", sendto, Sendto)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Jobs */
  lazy val Jobs = new TableQuery(tag => new Jobs(tag))

  /** Entity class storing rows of table Messages
   *  @param id Database column id SqlType(INT), AutoInc, PrimaryKey
   *  @param subject Database column subject SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param content Database column content SqlType(VARCHAR), Length(1000,true), Default(None)
   *  @param name Database column name SqlType(VARCHAR), Length(100,true), Default(None)
   *  @param email Database column email SqlType(VARCHAR), Length(60,true)
   *  @param username Database column username SqlType(VARCHAR), Length(254,true) */
  case class MessagesRow(id: Int, subject: Option[String] = None, content: Option[String] = None, name: Option[String] = None, email: String, username: String)
  /** GetResult implicit for fetching MessagesRow objects using plain SQL queries */
  implicit def GetResultMessagesRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[String]): GR[MessagesRow] = GR{
    prs => import prs._
    MessagesRow.tupled((<<[Int], <<?[String], <<?[String], <<?[String], <<[String], <<[String]))
  }
  /** Table description of table messages. Objects of this class serve as prototypes for rows in queries. */
  class Messages(_tableTag: Tag) extends Table[MessagesRow](_tableTag, "messages") {
    def * = (id, subject, content, name, email, username) <> (MessagesRow.tupled, MessagesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), subject, content, name, Rep.Some(email), Rep.Some(username)).shaped.<>({r=>import r._; _1.map(_=> MessagesRow.tupled((_1.get, _2, _3, _4, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column subject SqlType(VARCHAR), Length(254,true), Default(None) */
    val subject: Rep[Option[String]] = column[Option[String]]("subject", O.Length(254,varying=true), O.Default(None))
    /** Database column content SqlType(VARCHAR), Length(1000,true), Default(None) */
    val content: Rep[Option[String]] = column[Option[String]]("content", O.Length(1000,varying=true), O.Default(None))
    /** Database column name SqlType(VARCHAR), Length(100,true), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Length(100,varying=true), O.Default(None))
    /** Database column email SqlType(VARCHAR), Length(60,true) */
    val email: Rep[String] = column[String]("email", O.Length(60,varying=true))
    /** Database column username SqlType(VARCHAR), Length(254,true) */
    val username: Rep[String] = column[String]("username", O.Length(254,varying=true))

    /** Foreign key referencing Users (database name fk_messages_users1) */
    lazy val usersFk = foreignKey("fk_messages_users1", username, Users)(r => r.username, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Messages */
  lazy val Messages = new TableQuery(tag => new Messages(tag))

  /** Entity class storing rows of table Sendfrom
   *  @param id Database column id SqlType(INT), AutoInc, PrimaryKey
   *  @param name Database column name SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param address1 Database column address1 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param address2 Database column address2 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param postcode Database column postcode SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param notes Database column notes SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param username Database column username SqlType(VARCHAR), Length(254,true)
   *  @param visible Database column visible SqlType(BIT), Default(None) */
  case class SendfromRow(id: Int, name: Option[String] = None, address1: Option[String] = None, address2: Option[String] = None, postcode: Option[String] = None, notes: Option[String] = None, username: String, visible: Option[Boolean] = None)
  /** GetResult implicit for fetching SendfromRow objects using plain SQL queries */
  implicit def GetResultSendfromRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[String], e3: GR[Option[Boolean]]): GR[SendfromRow] = GR{
    prs => import prs._
    SendfromRow.tupled((<<[Int], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<[String], <<?[Boolean]))
  }
  /** Table description of table sendfrom. Objects of this class serve as prototypes for rows in queries. */
  class Sendfrom(_tableTag: Tag) extends Table[SendfromRow](_tableTag, "sendfrom") {
    def * = (id, name, address1, address2, postcode, notes, username, visible) <> (SendfromRow.tupled, SendfromRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), name, address1, address2, postcode, notes, Rep.Some(username), visible).shaped.<>({r=>import r._; _1.map(_=> SendfromRow.tupled((_1.get, _2, _3, _4, _5, _6, _7.get, _8)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column name SqlType(VARCHAR), Length(45,true), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Length(45,varying=true), O.Default(None))
    /** Database column address1 SqlType(VARCHAR), Length(254,true), Default(None) */
    val address1: Rep[Option[String]] = column[Option[String]]("address1", O.Length(254,varying=true), O.Default(None))
    /** Database column address2 SqlType(VARCHAR), Length(254,true), Default(None) */
    val address2: Rep[Option[String]] = column[Option[String]]("address2", O.Length(254,varying=true), O.Default(None))
    /** Database column postcode SqlType(VARCHAR), Length(45,true), Default(None) */
    val postcode: Rep[Option[String]] = column[Option[String]]("postcode", O.Length(45,varying=true), O.Default(None))
    /** Database column notes SqlType(VARCHAR), Length(254,true), Default(None) */
    val notes: Rep[Option[String]] = column[Option[String]]("notes", O.Length(254,varying=true), O.Default(None))
    /** Database column username SqlType(VARCHAR), Length(254,true) */
    val username: Rep[String] = column[String]("username", O.Length(254,varying=true))
    /** Database column visible SqlType(BIT), Default(None) */
    val visible: Rep[Option[Boolean]] = column[Option[Boolean]]("visible", O.Default(None))

    /** Foreign key referencing Clients (database name fk_sendfrom_clients1) */
    lazy val clientsFk = foreignKey("fk_sendfrom_clients1", username, Clients)(r => r.username, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Sendfrom */
  lazy val Sendfrom = new TableQuery(tag => new Sendfrom(tag))

  /** Entity class storing rows of table Sendto
   *  @param id Database column id SqlType(INT), AutoInc, PrimaryKey
   *  @param name Database column name SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param address1 Database column address1 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param address2 Database column address2 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param postcode Database column postcode SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param notes Database column notes SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param username Database column username SqlType(VARCHAR), Length(254,true)
   *  @param visible Database column visible SqlType(BIT), Default(None) */
  case class SendtoRow(id: Int, name: Option[String] = None, address1: Option[String] = None, address2: Option[String] = None, postcode: Option[String] = None, notes: Option[String] = None, username: String, visible: Option[Boolean] = None)
  /** GetResult implicit for fetching SendtoRow objects using plain SQL queries */
  implicit def GetResultSendtoRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[String], e3: GR[Option[Boolean]]): GR[SendtoRow] = GR{
    prs => import prs._
    SendtoRow.tupled((<<[Int], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<[String], <<?[Boolean]))
  }
  /** Table description of table sendto. Objects of this class serve as prototypes for rows in queries. */
  class Sendto(_tableTag: Tag) extends Table[SendtoRow](_tableTag, "sendto") {
    def * = (id, name, address1, address2, postcode, notes, username, visible) <> (SendtoRow.tupled, SendtoRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), name, address1, address2, postcode, notes, Rep.Some(username), visible).shaped.<>({r=>import r._; _1.map(_=> SendtoRow.tupled((_1.get, _2, _3, _4, _5, _6, _7.get, _8)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column name SqlType(VARCHAR), Length(45,true), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Length(45,varying=true), O.Default(None))
    /** Database column address1 SqlType(VARCHAR), Length(254,true), Default(None) */
    val address1: Rep[Option[String]] = column[Option[String]]("address1", O.Length(254,varying=true), O.Default(None))
    /** Database column address2 SqlType(VARCHAR), Length(254,true), Default(None) */
    val address2: Rep[Option[String]] = column[Option[String]]("address2", O.Length(254,varying=true), O.Default(None))
    /** Database column postcode SqlType(VARCHAR), Length(45,true), Default(None) */
    val postcode: Rep[Option[String]] = column[Option[String]]("postcode", O.Length(45,varying=true), O.Default(None))
    /** Database column notes SqlType(VARCHAR), Length(254,true), Default(None) */
    val notes: Rep[Option[String]] = column[Option[String]]("notes", O.Length(254,varying=true), O.Default(None))
    /** Database column username SqlType(VARCHAR), Length(254,true) */
    val username: Rep[String] = column[String]("username", O.Length(254,varying=true))
    /** Database column visible SqlType(BIT), Default(None) */
    val visible: Rep[Option[Boolean]] = column[Option[Boolean]]("visible", O.Default(None))

    /** Foreign key referencing Clients (database name fk_sendto_clients1) */
    lazy val clientsFk = foreignKey("fk_sendto_clients1", username, Clients)(r => r.username, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Sendto */
  lazy val Sendto = new TableQuery(tag => new Sendto(tag))

  /** Entity class storing rows of table Users
   *  @param username Database column username SqlType(VARCHAR), PrimaryKey, Length(254,true)
   *  @param password Database column password SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param name Database column name SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param surname Database column surname SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param iscourier Database column iscourier SqlType(BIT), Default(None)
   *  @param created Database column created SqlType(TIMESTAMP)
   *  @param email Database column email SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param phone Database column phone SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param address1 Database column address1 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param address2 Database column address2 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param postcode Database column postcode SqlType(VARCHAR), Length(45,true), Default(None)
   *  @param text1 Database column text1 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param text2 Database column text2 SqlType(VARCHAR), Length(254,true), Default(None)
   *  @param enabled Database column enabled SqlType(BIT), Default(None) */
  case class UsersRow(username: String, password: Option[String] = None, name: Option[String] = None, surname: Option[String] = None, iscourier: Option[Boolean] = None, created: java.sql.Timestamp, email: Option[String] = None, phone: Option[String] = None, address1: Option[String] = None, address2: Option[String] = None, postcode: Option[String] = None, text1: Option[String] = None, text2: Option[String] = None, enabled: Option[Boolean] = None)
  /** GetResult implicit for fetching UsersRow objects using plain SQL queries */
  implicit def GetResultUsersRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Option[Boolean]], e3: GR[java.sql.Timestamp]): GR[UsersRow] = GR{
    prs => import prs._
    UsersRow.tupled((<<[String], <<?[String], <<?[String], <<?[String], <<?[Boolean], <<[java.sql.Timestamp], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[Boolean]))
  }
  /** Table description of table users. Objects of this class serve as prototypes for rows in queries. */
  class Users(_tableTag: Tag) extends Table[UsersRow](_tableTag, "users") {
    def * = (username, password, name, surname, iscourier, created, email, phone, address1, address2, postcode, text1, text2, enabled) <> (UsersRow.tupled, UsersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(username), password, name, surname, iscourier, Rep.Some(created), email, phone, address1, address2, postcode, text1, text2, enabled).shaped.<>({r=>import r._; _1.map(_=> UsersRow.tupled((_1.get, _2, _3, _4, _5, _6.get, _7, _8, _9, _10, _11, _12, _13, _14)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column username SqlType(VARCHAR), PrimaryKey, Length(254,true) */
    val username: Rep[String] = column[String]("username", O.PrimaryKey, O.Length(254,varying=true))
    /** Database column password SqlType(VARCHAR), Length(254,true), Default(None) */
    val password: Rep[Option[String]] = column[Option[String]]("password", O.Length(254,varying=true), O.Default(None))
    /** Database column name SqlType(VARCHAR), Length(45,true), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Length(45,varying=true), O.Default(None))
    /** Database column surname SqlType(VARCHAR), Length(254,true), Default(None) */
    val surname: Rep[Option[String]] = column[Option[String]]("surname", O.Length(254,varying=true), O.Default(None))
    /** Database column iscourier SqlType(BIT), Default(None) */
    val iscourier: Rep[Option[Boolean]] = column[Option[Boolean]]("iscourier", O.Default(None))
    /** Database column created SqlType(TIMESTAMP) */
    val created: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("created")
    /** Database column email SqlType(VARCHAR), Length(254,true), Default(None) */
    val email: Rep[Option[String]] = column[Option[String]]("email", O.Length(254,varying=true), O.Default(None))
    /** Database column phone SqlType(VARCHAR), Length(45,true), Default(None) */
    val phone: Rep[Option[String]] = column[Option[String]]("phone", O.Length(45,varying=true), O.Default(None))
    /** Database column address1 SqlType(VARCHAR), Length(254,true), Default(None) */
    val address1: Rep[Option[String]] = column[Option[String]]("address1", O.Length(254,varying=true), O.Default(None))
    /** Database column address2 SqlType(VARCHAR), Length(254,true), Default(None) */
    val address2: Rep[Option[String]] = column[Option[String]]("address2", O.Length(254,varying=true), O.Default(None))
    /** Database column postcode SqlType(VARCHAR), Length(45,true), Default(None) */
    val postcode: Rep[Option[String]] = column[Option[String]]("postcode", O.Length(45,varying=true), O.Default(None))
    /** Database column text1 SqlType(VARCHAR), Length(254,true), Default(None) */
    val text1: Rep[Option[String]] = column[Option[String]]("text1", O.Length(254,varying=true), O.Default(None))
    /** Database column text2 SqlType(VARCHAR), Length(254,true), Default(None) */
    val text2: Rep[Option[String]] = column[Option[String]]("text2", O.Length(254,varying=true), O.Default(None))
    /** Database column enabled SqlType(BIT), Default(None) */
    val enabled: Rep[Option[Boolean]] = column[Option[Boolean]]("enabled", O.Default(None))
  }
  /** Collection-like TableQuery object for table Users */
  lazy val Users = new TableQuery(tag => new Users(tag))
}
