/**
 * Created by raitis on 06/08/15.
 */

package models.DAO

import models.auto_generated.Tables._

import scala.concurrent.Future



trait UsersDAOComponent {
  def insert(user: UsersRow): Future[Int]
  def update(username: String, user: UsersRow): Future[Int]
  def delete(username: String): Future[Int]
  def list(orderBy: String, filter: String = "%"): Future[Users]
  def findById(username: String): Future[UsersRow]
  def count: Future[Int]
}


//class UsersDAO extends UsersDAOComponent {
//
//  val users = TableQuery[Users]
//
//  def db: Database = Database.forDataSource(DB.getDataSource())
//
//  def filterQuery(username: String): Query[Users, UsersRow, Seq] =
//    users.filter(_.username === username)
//
//  def findById(username: String): Future[UsersRow] =
//    try db.run(filterQuery(username).result.head)
//    finally db.close
//
//  def insert(user: UsersRow): Future[Int] =
//    try db.run(users += user)
//    finally db.close
//
//  def update(username: String, user: UsersRow): Future[Int] =
//    try db.run(filterQuery(username).update(user))
//    finally db.close
//
//  def delete(username: String): Future[Int] =
//    try db.run(filterQuery(username).delete)
//    finally db.close
//
//}
